# RCon client
---
This is a desktop application made with Electron for controlling game servers based on Valve source rcon protocol remotely.

## Installation
1. Run `npm install`

### Technologies
* NodeJs
* Electron
* VueJs
* TailwindCSS