let mix = require('laravel-mix');
let tailwindcss = require('tailwindcss');
let postCssImport = require('postcss-import');


mix.postCss('src/assets/dev/css/app.css', 'src/assets/production/css', [
    postCssImport(),
    tailwindcss('./tailwind.js')
]);