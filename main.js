const {app, BrowserWindow} = require('electron')
const path = require('path')
const url = require('url')

let win

function createWindow () {
  // Create the browser window.
  win = new BrowserWindow({minWidth: 650, minHeight: 500, frame: true})

  // and load the index.html of the app.
  win.loadURL(url.format({
  pathname: path.join(__dirname, 'src/index.html'),
  protocol: 'file:',
  slashes: true
}))

// Open the DevTools.
//win.webContents.openDevTools()

// Emitted when the window is closed.
win.on('closed', () => {
  win = null
  })
}

app.on('ready', createWindow)

// Quit when all windows are closed.
app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
  app.quit()
  }
})

app.on('activate', () => {
  if (win === null) {
  createWindow()
  }
})

// In this file you can include the rest of your app's specific main process
// code. You can also put them in separate files and require them here.