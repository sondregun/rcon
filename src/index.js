const Packet = require('./Packet')
const net = require('net')

// Connection details
const ip = '127.0.0.1'
const port = 25575
const password = 'tintin'

let packet = new Packet()

// Establish connection with game server
let client = net.connect({ host: ip, port: port })

client.on('connect', () => {
    client.write(packet.encode(password, 3))
})

let app = new Vue({
    el: '#root',
    data: {
        newMessage: '',
        messages: []
    },

    methods: {
        addMessage() {
            client.write(packet.encode(this.newMessage))
            this.messages.push(this.newMessage)
            this.newMessage = ''
        }
    }
})

// Event: on recieved data, log it to console
client.on('data', (data) => {
    let res = packet.decode(data)
    if (res.type === 0) {
        app.messages.push(res.content)
    } else if (res.type === 2) {
        app.messages.push('Authenticated successfully')
    }
})
