const buffer = require('buffer').Buffer

module.exports = class Packet {
    constructor() {
        this.id = 0
    }

    // Decodes a network packet based on Source(Valve) protocol.
    decode(data) {
        let response = {
            size: data.readInt32LE(0),
            id: data.readInt32LE(4),
            type: data.readInt32LE(8),
            content: data.toString("ascii", 12, data.length - 2)
        }

        return response
    }

    // Encodes a network packet based on Source(Valve) protocol.
    encode(content, type = 2) {
        (this.id > 9000) ? this.id = 1 : this.id += 1;

        let length = Buffer.byteLength(content) + 14
        let packet = new Buffer(length)

        packet.writeInt32LE(length - 4, 0) // Size
        packet.writeInt32LE(this.id, 4)   // ID
        packet.writeInt32LE(type, 8)   // Type
        packet.write(content, 12, length - 2, "ascii") // Body
        packet.writeInt16LE(0, length - 2)  // Empty string

        return packet
    }
}